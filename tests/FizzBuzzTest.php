<?php
//TODO: Write FizzBuzz::check() to make tests work
// require_once __DIR__ . '/../FizzBuzz.php';

// class FizzBuzzTest extends PHPUnit\Framework\TestCase
// {
//   public function setUp(): void
//   {
//     // $foo = $this->getMockBuilder('FizzBuzz')
//     //     ->setMethods(array('check'))
//     //     ->getMock();
//     $this->fb = new FizzBuzz();

//   }

//   public function testGetFizz()
//   {
//     $expected = 'Fizz';
//     $input = 3;
//     $response = $this->fb->check($input);

//     $this->assertEquals($expected, $response);
//   }

//   public function testGetBuzz()
//   {
//     $expected = 'Buzz';
//     $input = 5;
//     $response = $this->fb->check($input);

//     $this->assertEquals($expected, $response);
//   }

//   public function testGetFizzBuzz()
//   {
//     $expected = 'FizzBuzz';
//     $input = 15;
//     $response = $this->fb->check($input);

//     $this->assertEquals($expected, $response);
//   }

//   function testPassThru()
//   {
//     $expected = '1';
//     $input = 1;
//     $response = $this->fb->check($input);

//     $this->assertEquals($expected, $response);
//   }


//   public function fizzBuzzProvider()
//   {
//     return [
//       [1, '1'],
//       [3, 'Fizz'],
//       [5, 'Buzz'],
//       [15, 'FizzBuzz']
//     ];
//   }

//   /**
//   * Test for our FizzBuzz object
//   *
//   * @dataProvider fizzBuzzProvider
//   */
//   public function testFizzBuzz($input, $expected)
//   {
//     $response = $this->fb->check($input);

//     $this->assertEquals($expected, $response);
//   }

//   /**
//   * Test for our FizzBuzz object
//   *
//   * @dataProvider \Helpers\Foo::dataProvider
//   */
//   public function testFizzBuzz2($input, $expected)
//   {
//     $response = $this->fb->check($input);

//     $this->assertEquals($expected, $response);
//   }
// }