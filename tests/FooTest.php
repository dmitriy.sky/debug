<?php

require_once __DIR__ . '/../Foo.php';

class FooTest extends PHPUnit\Framework\TestCase
{
  public function testProtectedBar()
  {
    $fooClass = new Foo;
    $reflectionFooClass = new ReflectionClass($fooClass);
    $reflectionBarMethod = $reflectionFooClass->getMethod('bar');
    $reflectionBarMethod->setAccessible(true);
    $reflectionBarMethod->invoke($fooClass, 'production');
    $expectedMessage = 'PROTECTED BAR';

    $this->assertAttributeEquals(
      $expectedMessage,
      'message',
      $fooClass,
      'Did not get expected message'
    );
  }
}