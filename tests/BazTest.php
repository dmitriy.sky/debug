<?php

require_once __DIR__ . '/../Baz.php';

class BazTest extends \PHPUnit\Framework\TestCase {

  public function testTrue() {
    // $test = new Baz();
    $this->assertTrue( true );
  }

  public function testThatBarMergesCorrectly()
  {
    $foo = $this->getMockBuilder('Foo')->getMock();
    $bar = $this->getMockBuilder('Bar')
        ->setMethods(array('getStatus', 'merge'))
        ->getMock();
    $bar->expects($this->once())
        ->method('getStatus')
        ->will($this->returnValue('merge-ready'));

     // Create our Baz object and then test our functionality
    $baz = new Baz($foo, $bar);
    $expectedResult = true;
    $testResult = $baz->mergeBar();

    $this->assertEquals(
      $expectedResult,
      $testResult,
      'Baz::mergeBar did not correctly merge our Bar object'
    );
  }

  public function testMergeOfBarDidNotHappen()
  {
    $foo = $this->getMockBuilder('Foo')->getMock();
    $bar = $this->getMockBuilder('Bar')
        ->setMethods(array('getStatus', 'pending'))
        ->getMock();
    $bar->expects($this->any())
        ->method('getStatus')
        ->will($this->returnValue('pending'));

    $baz = new Baz($foo, $bar);
    $testResult = $baz->mergeBar();

    $this->assertFalse(
      $testResult,
      'Bar with pending status should not be merged'
    );

  }

  public function testShowingUsingAt()
  {
    $foo = $this->getMockBuilder('Foo')
        // ->setMethods(array('abc'))
        ->getMock();
    $foo->expects($this->at(0))
        ->method('abc')
        ->will($this->returnValue(0));
    $foo->expects($this->at(1))
        ->method('abc')
        ->will($this->returnValue(1));
    $foo->expects($this->at(2))
        ->method('abc')
        ->will($this->returnValue(2));

    $this->assertEquals(0, $foo->abc());
    $this->assertEquals(1, $foo->abc());
    $this->assertEquals(2, $foo->abc());
  }

  public function testChangingReturnValuesBasedOnInput()
  {
    $foo = $this->getMockBuilder('Foo')
        // ->setMethods(array('bar', 'abc'))
        ->getMock();
    $valueMap = array(
      array(1, 'I'),
      array(4, 'IV'),
      array(10, 'X'),
    );
    $foo->expects($this->any())
        ->method('abc')
        ->will($this->returnValueMap($valueMap));

    $expectedResults = array('I', 'IV', 'X');
    $testResults = array();
    $testResults[] = $foo->abc(1);
    $testResults[] = $foo->abc(4);
    $testResults[] = $foo->abc(10);

    $this->assertEquals($expectedResults, $testResults);
  }

}