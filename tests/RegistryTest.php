<?php

require_once __DIR__ . '/../Registry.php';
class RegistryTest extends PHPUnit\Framework\TestCase
{
  public function testReturnsFalseForUnknownKey()
  {
    $registry = $this->getObjectForTrait('Registry');
    $response = $registry->get('foo');

    $this->assertFalse(
    $response,
    "Did not get expected false result for unknown key"
    );
  }
}