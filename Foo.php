<?php

class Foo
{
  protected $message;

  protected function bar($environment)
  {
    $this->message = "PROTECTED BAR";

    if ($environment == 'dev') {
      $this->message = 'CANDY BAR';
    }
  }

  public function abc()
  {
    return true;
  }
}