<?php

class Foo
{
  public function dataProvider()
  {
    return [
      'one' => [1, '1'],
      'fizz' => [3, 'Fizz'],
      'buzz' => [5, 'Buzz'],
      'fizzbuzz' => [15, 'FizzBuzz']
    ];
  }
}