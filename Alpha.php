<?php

class Alpha
{
  protected $beta;

  public function __construct($beta)
  {
    $this->beta = $beta;
  }

  public function calculate($deltas)
  {
    foreach ($deltas as $delta) {
      $this->beta->process($delta);
    }
  }
  // ...
}